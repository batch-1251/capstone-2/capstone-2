const mongoose=require('mongoose'); 

const userSchema=new mongoose.Schema(
	{
		email:{type:String, required:[true,"First name is required"]},
		password:{type:String, required:[true,"Password is required"]},
		isAdmin:{type:Boolean, default:false},
		orders:[
			{
			product:{
						name:{type:String, required:[true, "Product is required"]},
						description:{type:String, required:[true, "Description is required"]},
						price:{type:Number, required:[true, "Price is required"]},
					},
			purchasedOn:{type:Date, default:new Date()}
			},
			// {totalAmount:{type:Number, required:[true, "Price is required"]}}
		]
				 
			
	}
);

module.exports=mongoose.model("User",userSchema);