let jwt=require('jsonwebtoken');
let secret='EcommerceAPI';

//create a token
module.exports.createAccessToken=(user)=>{
	const data={id:user._id, email: user.email, isAdmin:user.Admin}
	return jwt.sign(data,secret,{});
}

//verify token
module.exports.verify=(req, res, next)=>{
	let token=req.headers.authorization
	if(typeof token!=="undefined"){
		token=token.slice(7, token.length);
		return jwt.verify(token,secret,(error,data)=>{
			if(error){return res.send({auth:"failed"})}
				else{next()}
		})

	}
}
//decode token
module.exports.decode=(token)=>{
		if (typeof token !== "undefined") {
		token = token.slice(7, token.length);
			return jwt.verify(token,secret,(error,data)=>{
				if(error){return null}
				else {return jwt.decode(token,{complete:true}).payload}
			})
	}

}