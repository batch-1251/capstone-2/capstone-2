const express=require('express');
const router=express.Router();


//controllers
const productController=require('./../controllers/productControllers');
const User=require("./../models/User");
const auth=require("./../auth");




//add product
router.post('/addProduct', auth.verify,(req, res) => {
	const userData=auth.decode(req.headers.authorization);
	User.findById(userData.id).then(result=>{
		if(result.isAdmin===true){
			productController.addProduct(req.body).then( result => res.send(result))}
			else{res.send("not an Admin")}
	})	
})

//update product
router.put('/:productId/edit', auth.verify, (req,res)=>{
	const userData=auth.decode(req.headers.authorization);
	User.findById(userData.id).then(result=>{
		if(result.isAdmin===true){
			productController.editProduct(req.params.productId, req.body).then(result=>res.send(result))}
						else{res.send("not an Admin")}
	})
})


//get all active product
router.get('/active', (req, res) => { 
	productController.getAllActive().then( result => res.send(result));
})


//get single product
router.get('/:productId', auth.verify, (req, res)=>{
	productController.getSingleProduct(req.params).then(result=>res.send(result));
})


//archive the product
router.put('/:productId/archive', auth.verify, (req,res)=>{
	const userData=auth.decode(req.headers.authorization);
	User.findById(userData.id).then(result=>{
		if(result.isAdmin===true){
	        productController.archiveProduct(req.params.productId).then(result=>res.send(result))}
			  else{res.send("not an Admin")}
	})
})


//unarchive the product
router.put('/:productId/unarchive', auth.verify, (req,res)=>{
	const userData=auth.decode(req.headers.authorization);
	User.findById(userData.id).then(result=>{
		if(result.isAdmin===true){
	       productController.unarchiveProduct(req.params.productId).then(result=>res.send(result))}
	       		else{res.send("not an Admin")}
	})
})




module.exports=router; 