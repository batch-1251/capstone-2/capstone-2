const express=require('express');
const router=express.Router();


const userController=require("./../controllers/userControllers");
const User=require("./../models/User");
const Product= require('./../models/Products')

const auth=require("./../auth"); 


//user registration and checks if email already exist
router.post("/register",(req,res)=>{
	userController.register(req.body).then(result=>res.send(result));
})
//login
router.post('/login',(req,res)=>{
	userController.login(req.body).then(result=>res.send(result))
})

//get all users
router.get('/allUsers',(req,res)=>{
	userController.allUsers().then(result=>res.send(result))
})



//set user as Admin
router.put('/:userId/setAsAdmin', auth.verify, (req,res)=>{
	const userData=auth.decode(req.headers.authorization);
	User.findById(userData.id).then(result=>{
		if(result.isAdmin===true){
	        userController.setAsAdmin(req.params.userId).then(result=>res.send(result))}
			  else{res.send(false)}
	})
})


//user orders
router.post('/checkout',auth.verify,(req, res)=>{
	const userData=auth.decode(req.headers.authorization);
	User.findById(userData.id).then(result=>{
		if(result.isAdmin===false){ 
			const data={userId:auth.decode(req.headers.authorization).id,
			           productId:req.body.productId

			       	}
		userController.order(data).then(result=>{res.send(result)})
		}else{res.send(false)}
	})
})


//retrieve user's orders
router.get('/myOrders', auth.verify,(req, res)=>{
	const data={userId:auth.decode(req.headers.authorization).id};
	userController.myOrders(data).then(result=>{res.send(result)})

})

//retrieve all orders
router.get('/orders', auth.verify,(req,res)=>{
	const userData=auth.decode(req.headers.authorization);
	User.findById(userData.id).then(result=>{
		if(result.isAdmin===true){ 
			userController.orders().then(result=>{res.send(result)})
				}else{res.send(false)}
	})
})


module.exports=router;