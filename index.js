
const express=require('express');
const mongoose=require('mongoose');
const cors=require('cors');

const PORT=process.env.PORT; //https://calm-temple-20940.herokuapp.com/
const app=express();


//middleware
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

//routes
let userRoutes=require("./routes/userRoutes")
let productRoutes=require("./routes/productRoutes")

//database
mongoose.connect("mongodb+srv://louie011:lowiidr011@cluster0.7ajhf.mongodb.net/e-commerce_capstone2?retryWrites=true&w=majority",
		{useNewUrlParser:true, useUnifiedTopology:true}
	).then(()=>console.log('Connected to Database')).catch((error)=>console.log(error));

//endpoint
app.use("/api/users", userRoutes);
app.use("/api/products", productRoutes);



app.listen(PORT, ()=>{console.log(`Server running on ${PORT}`)})