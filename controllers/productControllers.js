const Product= require('./../models/Products')

//add a product
module.exports.addProduct=(reqbody)=>{
	let newProduct= new Product({
		name:reqbody.name,
		description:reqbody.description,
		price:reqbody.price
	});
			return newProduct.save().then((product,error)=>{
				if(error){return false}
					else{return ("New product successfully added")}
			}) 
		
	
}

//update a product
module.exports.editProduct = (params, reqBody) => {

	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	return Product.findByIdAndUpdate(params, updatedProduct, {new: true})
	.then((product, error) => {return product})
}


//get all active products
module.exports.getAllActive=()=>{
	return Product.find({isActive: true}).then( result =>{return result}) 
}
 


//get single product
module.exports.getSingleProduct=(params)=>{
	return Product.findById(params.productId).then(result=>{
		return result
	})
}


//archive product
module.exports.archiveProduct=(params)=>{
	let updateActiveProduct={isActive:false}
	return Product.findByIdAndUpdate(params, updateActiveProduct, {new:true}).then((result,error)=>{
		if(error){return false}else{return true}
	})
}
//unarchive product
module.exports.unarchiveProduct=(params)=>{
	let updateActiveProduct={isActive:true}
	return Product.findByIdAndUpdate(params, updateActiveProduct, {new:true}).then((result,error)=>{
		if(error){return false}else{return true}
	})
}