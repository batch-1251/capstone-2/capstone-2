const User=require("./../models/User");
const Product=require('./../models/Products');
const bcrypt=require('bcrypt');
const auth=require('./../auth'); 



//register user
module.exports.register=(reqbody)=>{
	let newUser=new User({
		email:reqbody.email,
		password:bcrypt.hashSync(reqbody.password,10)
	})
	return User.find({email:reqbody.email}).then((result)=>{

		if(result.length!=0){return ("registration failed: email already exist")}

			else{

				return newUser.save().then((result,error)=>{
					if(error){return error}
						else{return true}
				})
			}
	    })
}

	
//User Authentication
module.exports.login=(reqbody)=>{
	
	return User.findOne({email:reqbody.email}).then((result)=>{
		if(result==null){return false}
			else{
				const isPasswordCorrect=bcrypt.compareSync(reqbody.password, result.password)
				if(isPasswordCorrect===true){
				 return {access:auth.createAccessToken(result.toObject())}
				}else{return false}
			}
	})
}

//set user as admin
module.exports.setAsAdmin=(params)=>{
	let updateAdmin={isAdmin:true}
	return User.findByIdAndUpdate(params, updateAdmin, {new:true}).then((result,error)=>{
		if(error){return false}else{return true}
	})
}

//get all users
module.exports.allUsers=()=>{
	return User.find().then(result=>{return result})
}


//Create user's order
module.exports.order =async(data) => { 

const orderProduct=await Product.findById(data.productId);
const createOrder=await User.findById(data.userId);
const buyProduct={
	product:{
			name:orderProduct.name,
			description:orderProduct.description,
			price:orderProduct.price
		}
};
	createOrder.orders.push(buyProduct)
	return createOrder.save().then((result,error)=>{if(error){return false}else{return true}})
}

//get user's order
module.exports.myOrders=(data)=>{
	return User.findById(data.userId).then((result,error)=>{if(error){return false}else{return [result.orders]}})
	

}
//retrieve all orders
module.exports.orders=()=>{
	return User.find({},'orders')
	.then((result,error)=>{
		if(error){return false}
			else{return result}})
}







